package lib;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "books")  
public class Book2 {
	
	private Integer id=0;
	private String title="";
	private String isbn="";
	private String author="";
	private String myabstract="";
	 @XmlElement 
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	 @XmlElement 
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	 @XmlElement 
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	 @XmlElement 
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	 @XmlElement 
	public String getMyabstract() {
		return myabstract;
	}
	public void setMyabstract(String myabstract) {
		this.myabstract = myabstract;
	}
}
