package lib;

import java.io.FileInputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.Properties;

public class ConnectionFactory {

	private static ConnectionFactory connectionFactory = null;
	private String jdbcUrl = "";
	private String DB = "";
	private String username = "";
	private String password = "";
	private Properties props=null;
	private String path="";

	private ConnectionFactory() {
		
			getinit();
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}

	public Connection getConnection()  {
		Connection conn = null;
		
		try {
			conn = DriverManager
					.getConnection(jdbcUrl + DB, username, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return conn;
	}

	public static ConnectionFactory getInstance() {
		if (connectionFactory == null) {
			connectionFactory = new ConnectionFactory();
		}
		return connectionFactory;
	}
	
	private void getinit() {
		// TODO Auto-generated method stub
		
		
		try {
			props = new Properties();
			path =  ConnectionFactory.class.getClassLoader().getResource("config.properties").toURI().getPath();

			 System.out.print("path===================="+path);
			 FileInputStream in = new FileInputStream(path);

			 props .load(in);


			jdbcUrl = props.getProperty("DatabaseIpAddress");
			DB = props.getProperty("DatabaseName");
			username = props.getProperty("LoginAccount");
			password = props.getProperty("LoginPassword");
			

			

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
	}
}