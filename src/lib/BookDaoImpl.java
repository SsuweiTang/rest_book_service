package lib;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class BookDaoImpl implements BookDao {

	Connection myConn = null;
	private Statement stat = null;
	private ResultSet rs = null;
	List<Book> listBook = null;
	private String selectSQL_getByIsbn = "";
	private Properties props = null;
	private String path = "";
	private String table = "";

	private Connection getConnection() throws SQLException {
		Connection conn;
		conn = ConnectionFactory.getInstance().getConnection();
		return conn;
	}

	@Override
	public List<Book> GetAllBook() {
		// TODO Auto-generated method stub
		
		getinit();
		try {
			selectSQL_getByIsbn = "select * from " + table ;
			System.out.println(selectSQL_getByIsbn);
			myConn = getConnection();
			stat = myConn.createStatement();
			
			rs = stat.executeQuery(selectSQL_getByIsbn);

			listBook = new ArrayList<Book>();
			Book bk = null;
			while (rs.next()) {

				bk = new Book();
				
				bk.setId(rs.getInt(1));
				bk.setIsbn(rs.getString(3));
				bk.setTitle(rs.getString(2));
				bk.setAuthor(rs.getString(4));
				bk.setMyabstract(rs.getString(5));
				listBook.add(bk);

			}

		} catch (SQLException e) {
			System.out.println("DropDB Exception :" + e.toString());
		} finally {
			Close();
		}
		return listBook;

		
	}

	@Override
	public List<Book> GetById(String isbn) {
		// TODO Auto-generated method stub
		getinit();
		try {
			selectSQL_getByIsbn = "select * from " + table + " where isbn="+ isbn;
			System.out.println(selectSQL_getByIsbn);
			myConn = getConnection();
			stat = myConn.createStatement();
			
			rs = stat.executeQuery(selectSQL_getByIsbn);

			listBook = new ArrayList<Book>();
			Book bk = null;
			while (rs.next()) {

				bk = new Book();
				
//				System.out.println("rs.getInt(1)"+rs.getInt(1));
				bk.setId(rs.getInt(1));
				bk.setIsbn(rs.getString(3));
				bk.setTitle(rs.getString(2));
				bk.setAuthor(rs.getString(4));
				bk.setMyabstract(rs.getString(5));
				listBook.add(bk);

			}

		} catch (SQLException e) {
			System.out.println("DropDB Exception :" + e.toString());
		} finally {
			Close();
		}
		return listBook;
	}

	private void getinit() {
		// TODO Auto-generated method stub

		try {
			props = new Properties();
			path = BookDaoImpl.class.getClassLoader()
					.getResource("config.properties").toURI().getPath();

			System.out.print("path====================" + path);
			FileInputStream in = new FileInputStream(path);

			props.load(in);

			table = props.getProperty("TableName");

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	private void Close() {
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}

			if (stat != null) {
				stat.close();
				stat = null;
			}
			if (myConn != null) {
				myConn.close();
				myConn = null;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Close Exception :" + e.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
