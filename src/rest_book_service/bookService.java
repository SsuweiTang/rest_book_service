package rest_book_service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import lib.Book;
import lib.Book2;
import lib.BookDaoImpl;

@Path("/bookservice")
public class bookService {
	
	private Book bk=null;
	private List<Book> list_book=null;
	private List<Book> Mylist_book=null;
	private  Map<String, Book> map_book = new HashMap<String, Book>();  
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getbookbyisbn/{isbn}")
	public List<Book> getBookByIsbn(@PathParam("isbn") String isbn) {
		// TODO Auto-generated method stub
	 
		BookDaoImpl bookdao = new BookDaoImpl();
		
		list_book=new ArrayList<Book>();
		list_book=bookdao.GetById(isbn);
		Mylist_book=new ArrayList<Book>();
	   
		

		if (list_book != null&&list_book.size()>0) {
			
			bk=new Book();
			
			for (Book b : list_book) {

				
				 
				 bk=new Book();
				 bk.setAuthor(b.getAuthor());
				 bk.setId(b.getId());
				 bk.setIsbn(b.getIsbn());
				 bk.setTitle(b.getTitle());
				 bk.setMyabstract(b.getMyabstract());

			}
			Mylist_book.add(bk);
			 map_book.put(bk.getIsbn(), bk);
		}
//		return bk;
//		return  new ArrayList<Book>(map_book.values());
		return list_book;
	}
	
	
	@GET
	@Path("getallbook/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Book> getAllBook() {
		
		BookDaoImpl bookdao = new BookDaoImpl();
		
		list_book=new ArrayList<Book>();
		list_book=bookdao.GetAllBook();
		
		return list_book;
	}
	
	
	


}
