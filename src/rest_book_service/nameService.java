package rest_book_service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/nameservice")
public class nameService {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("getMyName/{name}")
	public String getMyName(@PathParam("name") String name) {
		return "Hello, My name is "+name;
	}

}
